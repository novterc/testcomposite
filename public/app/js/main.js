(function () {

    var compositeList = [];

    function loadCompositeByArrayList(arrayList) {
        for (var key in arrayList) {
            var compositeArray = arrayList[key];
            var compositeItem = getCompositeByArray(compositeArray);
            compositeList.push(compositeItem);
        }
    }

    function getCompositeByArray(array) {
        var compositeItem = new composite(array.name);
        compositeItem.setCanEdit(false);
        for (var key in array.children) {
            var childArray = array.children[key];
            compositeItem.add(getCompositeByArray(childArray));
        }

        return compositeItem;
    }

    $.ajax({
        url: '/composite_get_all',
        method: 'GET',
        success: function (data) {
            if (data.status === true) {
                loadCompositeByArrayList(data.compositeList);
            }
        }
    });

    Vue.component('tree-component', {
        template: '#treeTemplate',
        props: ['composite', 'viewmode'],
        methods: {
            addChild: function (child) {
                child.add(new composite());
            }
        }
    });

    var app = new Vue({
        el: '#app',
        data: {
            compositeList: compositeList,
            selectComposite: null,
            viewMode: 'show'
        },
        methods: {
            createComposite: function () {
                var newComposite = new composite('new');
                this.selectComposite = newComposite;
                this.viewMode = 'edit';
                compositeList.push(newComposite);
            },
            saveComposite: function (composite) {
                composite.setCanEdit(false);
                this.viewMode = 'show';
                var compositeData = composite.getToData();
                $.ajax({
                    url: '/composite_create',
                    method: 'POST',
                    data: {
                        composite: compositeData
                    }
                });
            }
        }
    });

}());
