var composite = function (name) {
    var self = this;
    self.name = name;
    self.isCanEdit = true;
    self.children = [];

    self.add = function (item) {
        self.children.push(item);
        return self;
    };

    self.getChildren = function () {
        return self.children;
    };

    self.getChildrenCount = function () {
        return self.children.length;
    };

    self.getToData = function () {
        return {
            'name': self.name,
            'children': self.getChildrenToData()
        };
    };

    self.getChildrenToData = function () {
        var childrenData = [];
        var children = self.getChildren();
        for (var key in children) {
            var child = children[key];
            var childData = child.getToData();
            childData['key'] = key;
            childrenData.push(childData);
        }

        return childrenData;
    };

    self.setCanEdit = function (status) {
        self.isCanEdit = status;
    };

    return self;
};
