<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
          crossorigin="anonymous">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css"
          integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp"
          crossorigin="anonymous">
    <link rel="stylesheet" href="/app/css/style.css">

</head>
    <body>
        <script type="text/x-template" id="treeTemplate">
            <div>
                <div class="composit" v-if="viewmode == 'show'" >
                    <div class="composit-property">
                        <h5>@{{ composite.name }}</h5>
                    </div>
                    <div class="composit-child" v-for="child in composite.getChildren()">
                        <tree-component :viewmode="viewmode" :composite="child"/>
                    </div>
                </div>
                <div class="composit" v-if="viewmode == 'edit'" >
                    <div class="composit-property">
                        <input type="text" v-model="composite.name" placeholder="edit name" >
                        <button type="button" v-on:click="addChild(composite)">add child</button>
                    </div>
                    <div class="composit-child" v-for="child in composite.getChildren()">
                        <tree-component :viewmode="viewmode" :composite="child"/>
                    </div>
                </div>
            </div>
        </script>

        <div class="container bs-docs-container">
            <h1>hello</h1>
            <div id="app">

                <div class="row">
                    <div class="col-12">
                        <div class="list-group">
                            <a v-for="composite in compositeList" href="#"
                               class="list-group-item"
                               v-bind:class="[selectComposite == composite ? 'active' : '']"
                               v-on:click="selectComposite = composite">
                                    @{{ composite.name }}
                            </a>
                        </div>

                        <button type="button" class="btn btn-primary pull-right"  v-on:click="createComposite()">Add new</button>
                    </div>
                </div>


                <div class="row">
                    <div class="col-12">
                        <div v-if="selectComposite != null && selectComposite.isCanEdit == true">
                            <button type="button" class="btn"
                                    v-bind:class="[viewMode == 'show' ? 'btn-success' : 'btn-default']"
                                    v-on:click="viewMode = 'show'">View</button>
                            <button type="button" class="btn"
                                    v-bind:class="[viewMode == 'edit' ? 'btn-success' : 'btn-default']"
                                    v-on:click="viewMode = 'edit'">Edit</button>
                            <button type="button" class="btn btn-default"
                                    v-on:click="saveComposite(selectComposite)">Save</button>
                        </div>

                        <div class="composit-wrapper">
                            <tree-component :viewmode="viewMode" v-if="selectComposite != null" :composite="selectComposite"/>
                        </div>
                    </div>
                </div>
            </div>

        </div>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script type="application/javascript">
            $.ajaxSetup({ headers: { 'X-CSRF-TOKEN' : '{{ csrf_token() }}' } });
        </script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <script src="https://unpkg.com/vue"></script>
        <script src="/app/js/composite.js"></script>
        <script src="/app/js/main.js"></script>
    </body>
</html>