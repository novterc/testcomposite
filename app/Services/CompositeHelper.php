<?php

namespace App\Services;

use App\Models\Composite;

class CompositeHelper
{
    private $entity;
    private $children;

    public static function loadByArray(array $array, $parent = null)
    {
        $item = new self();
        $item->entity = Composite::create([
            'name' => (isset($array['name']) ? $array['name'] : ''),
            'parent' => $parent,
        ]);
        if (!empty($array['children'])) {
            $item->loadChildrenByArray($array['children']);
        }

        return $item;
    }

    public function loadChildrenByArray(array $childrenArray)
    {
        foreach ($childrenArray as $childArray) {
            $child = self::loadByArray($childArray, $this->getId());
            $this->addChild($child);
        }
    }

    public static function getAllLikeArray()
    {
        $compositeList = self::getAll();
        $compositeArray = [];
        if(!empty($compositeList)) {
            foreach ($compositeList as $composite) {
                $compositeArray[] = $composite->getArray();
            }
        }


        return $compositeArray;
    }

    public static function getAll()
    {
        $returnArray = [];
        $rootCompositeEntitys = Composite::getRoot()->get();
        if (!empty($rootCompositeEntitys)) {
            foreach ($rootCompositeEntitys as $entity) {
                $returnArray[] = self::loadByOrm($entity);
            }
        }

        return $returnArray;
    }

    public static function loadByOrm(Composite $entity)
    {
        $item = new self();
        $item->entity = $entity;
        $item->loadChildrenByOrm($entity);

        return $item;
    }

    public function loadChildrenByOrm(Composite $entity)
    {
        $childrenEntity = Composite::getByParentId($this->getId())->get();
        if(!empty($childrenEntity)) {
            foreach ($childrenEntity as $childEnitity) {
                $child = self::loadByOrm($childEnitity);
                $this->addChild($child);
            }
        }
    }

    public function getArray()
    {
        $compositeArray = [
            'name' => $this->getName(),
            'children' => [],
        ];
        if(!empty($this->children)) {
            foreach ($this->children as $child) {
                $compositeArray['children'][] = $child->getArray();
            }
        }

        return $compositeArray;
    }

    public function getName()
    {
        return $this->entity->name;
    }

    public function getId()
    {
        return $this->entity->id;
    }

    public function addChild(self $child)
    {
        $this->children[$child->getId()] = $child;
    }

}
