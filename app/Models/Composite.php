<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Composite extends Model
{
    protected $table = 'composite';
    protected $parentColumn = 'id';
    public $timestamps = false;
    protected $fillable = [
        'name', 'parent'
    ];

    public function scopeGetRoot($query)
    {
        return $query->where('parent', null);
    }

    public function scopeGetByParentId($query, $parentId)
    {
        return $query->where('parent', $parentId);
    }
}
