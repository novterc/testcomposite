<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\CompositeHelper;

class MainController extends Controller
{
    public function indexAction()
    {
        return view('main');
    }

    public function getAllCompositeAction()
    {
        $compositeList = CompositeHelper::getAllLikeArray();
        return response()->json([
            'status' => true,
            'compositeList' => $compositeList,
        ]);
    }

    public function createCompositeAction(Request $request)
    {
        if(($composite = $request->get('composite'))){
            CompositeHelper::loadByArray($composite);

            return response()->json([
                'status' => true,
            ]);
        } else {
            return response()->json([
                'status' => false,
                'reason' => 'Attribute the composite not found',
            ]);
        }
    }
}
